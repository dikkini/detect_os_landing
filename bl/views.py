# coding=utf-8

import ConfigParser

from pyramid.httpexceptions import HTTPMovedPermanently, HTTPNotFound
from uasparser2 import UASparser


def setRedirect(request):
    config = ConfigParser.ConfigParser()
    candidates = ['C:\devspace\python\kalinin-project\landingpage/bl\config.ini', ]
    config.read(candidates)

    serverWithPort = request.host

    data = getOS(request.user_agent)
    keywords = dict(config.items(serverWithPort))
    for keyword in keywords:
        if keyword.lower() in data['os_name'].lower():
            redirectUrl = config.get(serverWithPort, keyword)
            return HTTPMovedPermanently(redirectUrl)
    return HTTPNotFound()


def getOS(user_agent):
    uas_parser = UASparser('C:\devspace\python\kalinin-project\landingpage/bl\cache')
    result = uas_parser.parse(user_agent)
    return result







